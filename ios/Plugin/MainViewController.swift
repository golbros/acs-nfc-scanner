//
//  MainViewController.swift
//  scanner-app
//
//  Created by Marcel Golob on 09.08.21.
//

import Foundation
import UIKit




class MainViewController: UIViewController {
    
    
    var scannerLabel: UILabel!
    var lastScanLabel: UILabel!
    
    var scanHelper = ScannerHelper.shared
    
    override func loadView() {
        // super.loadView()   // DO NOT CALL SUPER
        
        view = UIView()
        view.backgroundColor = UIColor.white
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        
        let scanButton = UIButton(type: .system)
        scanButton.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        scanButton.setTitle("select reader", for: .normal)
        stackView.addArrangedSubview(scanButton)
        
        scannerLabel = UILabel()
        scannerLabel.translatesAutoresizingMaskIntoConstraints = false
        scannerLabel.text = "<no reader selected>"
        scannerLabel.textColor = UIColor.systemPink
        scannerLabel.textAlignment = .center
        stackView.addArrangedSubview(scannerLabel)
        
        
        let cardButton = UIButton(type: .system)
        cardButton.addTarget(self, action: #selector(readCard(_:)), for: .touchUpInside)
        cardButton.setTitle("card scan test", for: .normal)
        stackView.addArrangedSubview(cardButton)
        
        lastScanLabel = UILabel()
        lastScanLabel.translatesAutoresizingMaskIntoConstraints = false
        lastScanLabel.text = "<no card scanned>"
        lastScanLabel.textAlignment = .center
        lastScanLabel.textColor = UIColor.systemPink
        stackView.addArrangedSubview(lastScanLabel)
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveTerminalName(_:)), name: .didReceiveTerminalName, object: nil)
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        scanHelper.showListController(rootVC: self)
    }
    
    @IBAction func readCard(_ sender: UIButton) {
        let result = scanHelper.scanCard()
        lastScanLabel.text = result;
    }
    
    @objc func onDidReceiveTerminalName(_ notification: Notification) {
        scannerLabel.text = notification.object as? String
    }
}


extension Notification.Name {
    static let didReceiveTerminalName = Notification.Name("didReceiveTerminalName")
}
