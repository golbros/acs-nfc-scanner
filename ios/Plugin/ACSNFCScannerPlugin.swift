import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(ACSNFCScannerPlugin)
public class ACSNFCScannerPlugin: CAPPlugin {
    var call: CAPPluginCall?
    var mainViewController: MainViewController?

    @objc func showScannerList(_ call: CAPPluginCall) {
        self.call = call
        DispatchQueue.main.async {
            self.mainViewController = MainViewController()
            self.bridge?.viewController?.present(self.mainViewController!, animated: true, completion: nil)
         }
    }
    
    @objc func scanCard(_ call: CAPPluginCall) {
        let result = ScannerHelper.shared.scanCard()
        call.resolve(["cardHex": result])
    }
}
