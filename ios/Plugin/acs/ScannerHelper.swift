//
//  ScannerHelper.swift
//  scanner-app
//
//  Created by Marcel Golob on 09.08.21.
//

import Foundation
import ACSSmartCardIO
import SmartCardIO



class ScannerHelper: BluetoothTerminalManagerDelegate, TerminalListViewControllerDelegate {
    
    
    static let shared = ScannerHelper()
    
    var terminalListViewController: TerminalListViewController = TerminalListViewController()
    
    var firstRun = true
    let manager = BluetoothSmartCard.shared.manager
    let factory = BluetoothSmartCard.shared.factory
    var cardTerminal: CardTerminal? = nil
    var card : Card? = nil
    
    init() {
        manager.delegate = self
    }
    
    func showListController(rootVC : UIViewController) {
        
        if(cardTerminal != nil) {
            NotificationCenter.default.post(name: .didReceiveTerminalName, object: self.cardTerminal?.name)
        }
        
        terminalListViewController.manager = manager
        terminalListViewController.terminal = cardTerminal
        terminalListViewController.delegate = self
        rootVC.present(terminalListViewController, animated: true, completion: nil)
        startScan()
        
    }
    func startScan () {
        manager.startScan(terminalType: .acr1255uj1v2)
    }
    
    func stopScan() {
        manager.stopScan()
    }
    
    func bluetoothTerminalManagerDidUpdateState(_ manager: BluetoothTerminalManager) {
        var message = ""
        
        switch manager.centralManager.state {
        
        case .unknown, .resetting:
            message = "The update is being started. Please wait until Bluetooth is ready."
            
        case .unsupported:
            message = "This device does not support Bluetooth low energy."
            
        case .unauthorized:
            message = "This app is not authorized to use Bluetooth low energy."
            
        case .poweredOff:
            if !firstRun {
                message = "You must turn on Bluetooth in Settings in order to use the reader."
            }
            
        default:
            break
        }
        
        if !message.isEmpty {
            
            // TODO: Show the message.
            // ...
        }
        
        firstRun = false
    }
    
    func scanCard() -> String {
        
        var didScan = false
        
        repeat {
            do {
                
                
                let command: [UInt8] = Hex.toByteArray(hexString: "FF CA 00 00 00")
                
                card = try cardTerminal?.connect(protocolString: "*")
                let channel = try card!.basicChannel()
                let commandAPDU = try CommandAPDU(apdu: command)
                let responseAPDU = try channel.transmit(apdu: commandAPDU)
                
                let uid = Hex.toHexString(buffer: responseAPDU.data)
                
                try card?.disconnect(reset: true)
                try manager.disconnect(terminal: cardTerminal!);
                didScan = true
                return uid;
                
            } catch {
                
            }
        } while (!didScan)
        
        
    }
    
    func bluetoothTerminalManager(_ manager: BluetoothTerminalManager, didDiscover terminal: CardTerminal) {
        
        
        if !terminalListViewController.terminals.contains(
            where: { $0 === terminal }) {
            
            terminalListViewController.terminals.append(terminal)
            DispatchQueue.main.async {
                self.terminalListViewController.tableView.reloadData()
            }
            
        }
        
    }
    
    func terminalListViewController(_ terminalListViewController: TerminalListViewController, didSelectTerminal terminal: CardTerminal) {
        print(terminal.name)
        self.cardTerminal = terminal
        manager.stopScan()
        
        NotificationCenter.default.post(name: .didReceiveTerminalName, object: self.cardTerminal?.name)
        
    }
    
    
}

class Hex {
    
    /// Converts the byte array to HEX string.
    ///
    /// - Parameter buffer: the buffer
    /// - Returns: the HEX string
    static func toHexString(buffer: [UInt8]) -> String {
        
        var bufferString = ""
        
        for i in 0..<buffer.count {
            if i == 0 {
                bufferString += String(format: "%02X", buffer[i])
            } else {
                bufferString += String(format: " %02X", buffer[i])
            }
        }
        
        return bufferString
    }
    
    /// Converts the HEX string to byte array.
    ///
    /// - Parameter hexString: the HEX string
    /// - Returns: the byte array
    static func toByteArray(hexString: String) -> [UInt8] {
        
        var byteArray = [UInt8]()
        var first = true
        var value: UInt32 = 0
        var byte: UInt8 = 0
        
        let digit0: Unicode.Scalar = "0"
        let digit9: Unicode.Scalar = "9"
        let letterA: Unicode.Scalar = "A"
        let letterF: Unicode.Scalar = "F"
        let lettera: Unicode.Scalar = "a"
        let letterf: Unicode.Scalar = "f"
        
        // For each HEX character, convert it to byte.
        for c in hexString.unicodeScalars {
            
            if c >= digit0 && c <= digit9 {
                value = c.value - digit0.value
            } else if c >= letterA && c <= letterF {
                value = c.value - letterA.value + 10
            } else if c >= lettera && c <= letterf {
                value = c.value - lettera.value + 10
            } else {
                continue
            }
            
            if (first) {
                
                byte = UInt8(value << 4)
                
            } else {
                
                byte |= UInt8(value)
                byteArray.append(byte)
            }
            
            first = !first
        }
        
        return byteArray
    }
}
