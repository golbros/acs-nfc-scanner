import { registerPlugin } from '@capacitor/core';

import type { ACSNFCScannerPlugin } from './definitions';

const ACSNFCScanner = registerPlugin<ACSNFCScannerPlugin>('ACSNFCScanner', { });

export * from './definitions';
export { ACSNFCScanner };
