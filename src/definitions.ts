export interface ACSNFCScannerPlugin {
  showScannerList(): Promise<{ scannerName: string }>;
  scanCard(): Promise<{cardHex: string}>
}
