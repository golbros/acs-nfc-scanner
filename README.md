# @johnbraum/acs-nfc-scanner

Scans NFC cards with the acs scanners for iOS

## Install

```bash
npm install @johnbraum/acs-nfc-scanner
npx cap sync
```

## API

<docgen-index>

* [`showScannerList()`](#showscannerlist)
* [`scanCard()`](#scancard)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### showScannerList()

```typescript
showScannerList() => any
```

**Returns:** <code>any</code>

--------------------


### scanCard()

```typescript
scanCard() => any
```

**Returns:** <code>any</code>

--------------------

</docgen-api>
